# FREEDOM-RANK-SELLERS

## Objetivo
Projeto responsável pela lista de sellers ordenadas de acordo com critérios de prioridade

## Arquitetura
* Arquitetura de Referência Netshoes https://github.com/netshoes/architecture-reference

## Tecnologias
* Spring Cloud
* Undertown
* MongoDB

## Formatação do Código
* Google Java Format https://github.com/google/google-java-format

## Toogle Feature
* FF4J endpoint  GET /ff4j-console/