package com.alefh.ranksellers.gateways.mongodb;

import br.com.six2six.fixturefactory.Fixture;
import com.alefh.ranksellers.domains.RankedSellers;
import com.alefh.ranksellers.gateways.mongodb.repositories.RankedSellersRepository;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;
import com.alefh.ranksellers.test.support.TestSupport;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

public class RankedSellersGatewayImplTest extends TestSupport {

  @InjectMocks private RankedSellersGatewayImpl rankedSellersGateway;

  @Mock private RankedSellersRepository rankedSellersRepository;

  private RankedSellers rankedSellers;

  @Override
  public void init() throws Exception {
    rankedSellers = Fixture.from(RankedSellers.class).gimme(FixtureCoreTemplates.VALID.name());
  }

  @Test
  public void save() throws Exception {
    rankedSellersGateway.save(rankedSellers);
    Mockito.verify(rankedSellersRepository).save(rankedSellers);
  }
}
