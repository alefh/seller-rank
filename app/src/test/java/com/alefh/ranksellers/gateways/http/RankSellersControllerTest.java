package com.alefh.ranksellers.gateways.http;

import static br.com.six2six.fixturefactory.Fixture.from;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.alefh.ranksellers.domains.RankedSellers;
import com.alefh.ranksellers.gateways.http.configurations.AbstractHttpTest;
import com.alefh.ranksellers.gateways.http.controllers.RankSellersController;
import com.alefh.ranksellers.gateways.http.resources.ListSkusResource;
import com.alefh.ranksellers.gateways.http.resources.request.SkuRequest;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;
import com.alefh.ranksellers.usecases.ProcessRankSellers;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(classes = RankSellersController.class)
public class RankSellersControllerTest extends AbstractHttpTest {

  @Autowired private RankSellersController rankSellersController;

  @MockBean private ProcessRankSellers processRankSellers;

  private MockMvc mockMvc;

  private ObjectMapper mapper;

  @Before
  public void setup() {
    FixtureFactoryLoader.loadTemplates("com.alefh.ranksellers.templates");
    mockMvc = buildMockMvcWithBusinessExecptionHandler(rankSellersController);
    mapper = new ObjectMapper();
  }

  @Test
  public void processRankSellers() throws Exception {
    RankedSellers rankedSellers =
        from(RankedSellers.class).gimme(FixtureCoreTemplates.VALID.name());

    ListSkusResource<SkuRequest> request =
        from(ListSkusResource.class).gimme(FixtureCoreTemplates.VALID.name());
    String json = mapper.writeValueAsString(request);

    Mockito.when(processRankSellers.execute(Matchers.anyString(), Matchers.anyString(), anyList()))
        .thenReturn(Lists.newArrayList(rankedSellers));

    MvcResult mvcResult =
        mockMvc
            .perform(
                MockMvcRequestBuilders.post("/api/v1/countries/BR/stores/LS/rank")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
            .andReturn();
    verify(processRankSellers, Mockito.times(1)).execute(anyString(), anyString(), anyList());
    assertThat(mvcResult.getResponse().getStatus(), equalTo(200));
    assertTrue(isNotBlank(mvcResult.getResponse().getContentAsString()));
  }

  @Test
  public void processRankSellersWhenBadRequest() throws Exception {
    ListSkusResource<SkuRequest> request =
        from(ListSkusResource.class).gimme(FixtureCoreTemplates.INVALID.name());

    String json = mapper.writeValueAsString(request);

    MvcResult mvcResult =
        mockMvc
            .perform(
                MockMvcRequestBuilders.post("/api/v1/countries/BR/stores/LS/rank")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
            .andReturn();
    assertThat(mvcResult.getResponse().getStatus(), equalTo(400));
  }
}
