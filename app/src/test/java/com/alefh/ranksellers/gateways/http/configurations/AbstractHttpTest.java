package com.alefh.ranksellers.gateways.http.configurations;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.alefh.ranksellers.gateways.http.CustomExceptionHandler;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebAppConfiguration
public abstract class AbstractHttpTest {

  protected MockMvc buildMockMvcWithBusinessExecptionHandler(final Object controller) {
    return standaloneSetup(controller).setControllerAdvice(new CustomExceptionHandler()).build();
  }
}
