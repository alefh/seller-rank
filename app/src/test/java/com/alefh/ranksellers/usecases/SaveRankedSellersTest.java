package com.alefh.ranksellers.usecases;

import br.com.six2six.fixturefactory.Fixture;
import com.alefh.ranksellers.test.support.TestSupport;
import com.alefh.ranksellers.domains.RankedSellers;
import com.alefh.ranksellers.gateways.RankedSellersGateway;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

public class SaveRankedSellersTest extends TestSupport {

  @InjectMocks private SaveRankedSellers saveRankedSellers;

  @Mock private RankedSellersGateway rankedSellersGateway;

  private RankedSellers rankedSellers;

  @Override
  public void init() throws Exception {
    rankedSellers = Fixture.from(RankedSellers.class).gimme(FixtureCoreTemplates.VALID.name());
  }

  @Test
  public void execute() throws Exception {
    saveRankedSellers.execute(rankedSellers);
    Mockito.verify(rankedSellersGateway).save(rankedSellers);
  }
}
