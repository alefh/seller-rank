package com.alefh.ranksellers.templates.gateways;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.alefh.ranksellers.gateways.http.resources.request.SellerRequest;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;

public class SellerRequestTemplate implements TemplateLoader {

  @Override
  public void load() {
    Fixture.of(SellerRequest.class)
        .addTemplate(
            FixtureCoreTemplates.VALID.name(),
            new Rule() {
              {
                add("id", random("0", "15", "7"));
                add("name", random("Loja", "Lojista"));
              }
            });
  }
}
