package com.alefh.ranksellers.templates.gateways;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.alefh.ranksellers.gateways.http.resources.ListSkusResource;
import com.alefh.ranksellers.gateways.http.resources.request.SkuRequest;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;

public class ListSkusResourceTemplate implements TemplateLoader {

  @Override
  public void load() {
    Fixture.of(ListSkusResource.class)
        .addTemplate(
            FixtureCoreTemplates.VALID.name(),
            new Rule() {
              {
                add("skus", has(3).of(SkuRequest.class, FixtureCoreTemplates.VALID.name()));
              }
            });

    Fixture.of(ListSkusResource.class)
        .addTemplate(
            FixtureCoreTemplates.INVALID.name(),
            new Rule() {
              {
                add("skus", has(3).of(SkuRequest.class, FixtureCoreTemplates.INVALID.name()));
              }
            });
  }
}
