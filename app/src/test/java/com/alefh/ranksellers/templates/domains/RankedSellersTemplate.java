package com.alefh.ranksellers.templates.domains;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.alefh.ranksellers.domains.Seller;
import com.alefh.ranksellers.domains.RankedSellers;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;

public class RankedSellersTemplate implements TemplateLoader {

  @Override
  public void load() {
    Fixture.of(RankedSellers.class)
        .addTemplate(
            FixtureCoreTemplates.VALID.name(),
            new Rule() {
              {
                add("sku", random("D14-5682-582-38", "J11-5682-122-40"));
                add("sellers", has(3).of(Seller.class, FixtureCoreTemplates.VALID.name()));
              }
            });
  }
}
