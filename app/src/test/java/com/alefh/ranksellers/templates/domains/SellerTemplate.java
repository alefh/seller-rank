package com.alefh.ranksellers.templates.domains;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.alefh.ranksellers.domains.Seller;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;

public class SellerTemplate implements TemplateLoader {

  @Override
  public void load() {
    Fixture.of(Seller.class)
        .addTemplate(
            FixtureCoreTemplates.VALID.name(),
            new Rule() {
              {
                add("id", random("0", "15", "7"));
                add("name", random("Loja", "Lojista"));
              }
            });
  }
}
