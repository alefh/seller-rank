package com.alefh.ranksellers.templates.gateways;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.alefh.ranksellers.gateways.http.resources.request.PriceRequest;
import com.alefh.ranksellers.gateways.http.resources.request.SkuRequest;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;

public class SkuRequestTemplate implements TemplateLoader {

  @Override
  public void load() {
    Fixture.of(SkuRequest.class)
        .addTemplate(
            FixtureCoreTemplates.VALID.name(),
            new Rule() {
              {
                add("sku", random("D14-5682-582-38", "J11-5682-122-40", "J11-5682-122-42"));
                add("available", random(Boolean.class));
                add("prices", has(3).of(PriceRequest.class, FixtureCoreTemplates.VALID.name()));
              }
            });

    Fixture.of(SkuRequest.class)
        .addTemplate(
            FixtureCoreTemplates.INVALID.name(),
            new Rule() {
              {
                add("available", random(Boolean.class));
                add("prices", has(3).of(PriceRequest.class, FixtureCoreTemplates.VALID.name()));
              }
            });
  }
}
