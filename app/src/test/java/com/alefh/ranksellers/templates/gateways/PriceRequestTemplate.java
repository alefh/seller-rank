package com.alefh.ranksellers.templates.gateways;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.alefh.ranksellers.gateways.http.resources.request.PriceRequest;
import com.alefh.ranksellers.templates.FixtureCoreTemplates;
import com.alefh.ranksellers.gateways.http.resources.request.SellerRequest;

public class PriceRequestTemplate implements TemplateLoader {

  @Override
  public void load() {
    Fixture.of(PriceRequest.class)
        .addTemplate(
            FixtureCoreTemplates.VALID.name(),
            new Rule() {
              {
                add("available", random(Boolean.class));
                add("saleInCents", random("2354"));
                add("listInCents", random("2354"));
                add("discountPrice", random("2354"));
                add("discountPercent", random("2354"));
                add("seller", one(SellerRequest.class, FixtureCoreTemplates.VALID.name()));
              }
            });
  }
}
