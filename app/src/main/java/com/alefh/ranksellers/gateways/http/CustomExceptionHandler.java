package com.alefh.ranksellers.gateways.http;

import com.alefh.ranksellers.gateways.http.resources.response.ErrorResponse;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice(value = "com.alefh.ranksellers.gateways.http")
public class CustomExceptionHandler {

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public HttpEntity<ErrorResponse> handlerValidationException(
      final MethodArgumentNotValidException ex) {
    log.debug(ex.getMessage(), ex);
    BindingResult bindingResult = ex.getBindingResult();
    List<FieldError> fieldErrors = bindingResult.getFieldErrors();
    ErrorResponse errorResponse = processFieldErrors(fieldErrors);
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
    return new ResponseEntity<>(errorResponse, responseHeaders, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({ServletRequestBindingException.class, IllegalArgumentException.class})
  public HttpEntity<ErrorResponse> handlerMissingServletRequestParameterException(
      final Exception ex) {
    log.debug(ex.getMessage(), ex);
    ErrorResponse errorResponse = new ErrorResponse(Collections.singletonList(ex.getMessage()));
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
    return new ResponseEntity<>(errorResponse, responseHeaders, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public void handlerHttpMessageNotReadableException(final HttpMessageNotReadableException ex) {
    log.error(ex.getMessage(), ex);
  }

  @ExceptionHandler({HttpMediaTypeNotSupportedException.class})
  @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
  public void handlerHttpMediaTypeNotSupportedException(
      final HttpMediaTypeNotSupportedException ex) {
    log.debug(ex.getMessage(), ex);
  }

  @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
  @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  public void handlerHttpRequestMethodNotSupportedException(
      final HttpRequestMethodNotSupportedException ex) {
    log.debug(ex.getMessage(), ex);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public HttpEntity<ErrorResponse> handlerResourceNotFoundException(
      final ResourceNotFoundException ex) {
    log.debug(ex.getMessage(), ex);
    ErrorResponse errorResponse = new ErrorResponse();
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8);
    return new ResponseEntity<>(errorResponse, responseHeaders, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler({Exception.class, Error.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public void handlerException(final Exception ex) {
    log.error(ex.getMessage(), ex);
  }

  private ErrorResponse processFieldErrors(final List<FieldError> fieldErrors) {
    ErrorResponse errorResponse = new ErrorResponse();
    for (FieldError fieldError : fieldErrors) {
      String localizedErrorMessage = fieldError.getDefaultMessage();
      errorResponse.addError(fieldError.getField().concat(":").concat(localizedErrorMessage));
    }
    return errorResponse;
  }
}
