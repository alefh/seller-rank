package com.alefh.ranksellers.gateways;

import com.alefh.ranksellers.domains.RankedSellers;

public interface RankedSellersGateway {

  RankedSellers save(RankedSellers rankedSellers);
}
