package com.alefh.ranksellers.gateways.mongodb.repositories;

import com.alefh.ranksellers.domains.RankedSellers;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RankedSellersRepository extends MongoRepository<RankedSellers, String> {}
