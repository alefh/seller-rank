package com.alefh.ranksellers.gateways.http.controllers;

import static java.util.stream.Collectors.toList;

import com.alefh.ranksellers.gateways.http.resources.ListSkusResource;
import com.alefh.ranksellers.gateways.http.resources.request.SkuRequest;
import com.alefh.ranksellers.gateways.http.resources.response.RankedSellersResponse;
import com.alefh.ranksellers.usecases.ProcessRankSellers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1")
@Api(
  value = "RankSellers",
  description = "Rest API for manipulate rank of sellers",
  produces = MediaType.APPLICATION_JSON_VALUE
)
@AllArgsConstructor
public class RankSellersController {

  private ProcessRankSellers processRankSellers;

  @RequestMapping(
    value = "/countries/{country}/stores/{storeCode}/rank",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(value = "Process a rank of sellers.")
  public ResponseEntity<ListSkusResource<RankedSellersResponse>> processRank(
      @PathVariable final String country,
      @PathVariable final String storeCode,
      @RequestBody @Valid final ListSkusResource<SkuRequest> listSkuRequest) {
    List<RankedSellersResponse> response =
        RankedSellersResponse.toList(
            processRankSellers.execute(
                country,
                storeCode,
                listSkuRequest.getSkus().stream().map(SkuRequest::toDomain).collect(toList())));
    return ResponseEntity.ok(new ListSkusResource<>(response));
  }
}
