package com.alefh.ranksellers.gateways.http.resources.response;

import com.alefh.ranksellers.domains.Seller;
import com.alefh.ranksellers.domains.RankedSellers;

import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class RankedSellersResponse {

  private String sku;
  private List<SellerResponse> rankedSellers;

  public RankedSellersResponse(final RankedSellers rankedSellers) {
    setSku(rankedSellers.getSku());
    setRankedSellers(
        rankedSellers.getSellers().stream().map(this::mapSeller).collect(Collectors.toList()));
  }

  public static List<RankedSellersResponse> toList(final List<RankedSellers> listRankedSellers) {
    return listRankedSellers.stream().map(RankedSellersResponse::new).collect(Collectors.toList());
  }

  private SellerResponse mapSeller(final Seller seller) {
    return new SellerResponse(seller.getId(), seller.getName());
  }
}
