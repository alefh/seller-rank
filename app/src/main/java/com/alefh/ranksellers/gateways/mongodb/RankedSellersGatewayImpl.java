package com.alefh.ranksellers.gateways.mongodb;

import com.alefh.ranksellers.domains.RankedSellers;
import com.alefh.ranksellers.gateways.RankedSellersGateway;
import com.alefh.ranksellers.gateways.mongodb.repositories.RankedSellersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class RankedSellersGatewayImpl implements RankedSellersGateway {

  private RankedSellersRepository rankedSellersRepository;

  @Override
  public RankedSellers save(RankedSellers rankedSellers) {
    return rankedSellersRepository.save(rankedSellers);
  }
}
