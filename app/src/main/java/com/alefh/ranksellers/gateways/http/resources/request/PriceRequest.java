package com.alefh.ranksellers.gateways.http.resources.request;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class PriceRequest {

  private static final String PATTERN_INTEGER = "\\d+";

  @NotNull(message = "{validation.field.notnull}")
  private Boolean available;

  @NotNull(message = "{validation.field.notnull}")
  @Pattern(regexp = PATTERN_INTEGER, message = "{validation.field.numeric}")
  private String saleInCents;

  @NotNull(message = "{validation.field.notnull}")
  @Pattern(regexp = PATTERN_INTEGER, message = "{validation.field.numeric}")
  private String listInCents;

  @NotNull(message = "{validation.field.notnull}")
  @Pattern(regexp = PATTERN_INTEGER, message = "{validation.field.numeric}")
  private String discountPrice;

  @NotNull(message = "{validation.field.notnull}")
  @Pattern(regexp = PATTERN_INTEGER, message = "{validation.field.numeric}")
  private String discountPercent;

  @Valid
  @NotNull(message = "{validation.field.notnull}")
  private SellerRequest seller;
}
