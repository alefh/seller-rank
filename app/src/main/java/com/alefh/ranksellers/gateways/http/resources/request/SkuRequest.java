package com.alefh.ranksellers.gateways.http.resources.request;

import static java.util.stream.Collectors.toList;

import com.alefh.ranksellers.domains.Seller;
import com.alefh.ranksellers.domains.Sku;
import com.alefh.ranksellers.domains.Price;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SkuRequest {

  @NotEmpty(message = "{validation.field.notnull}")
  private String sku;

  @NotNull(message = "{validation.field.notnull}")
  private Boolean available;

  @Valid @NotEmpty private List<PriceRequest> prices;

  public Sku toDomain() {
    Sku sku = new Sku();
    sku.setSku(this.getSku());
    sku.setAvailable(this.getAvailable());
    sku.setPrices(this.getPrices().stream().map(this::mapPrice).collect(toList()));
    return sku;
  }

  private Price mapPrice(final PriceRequest priceRequest) {
    Price price = new Price();
    price.setAvailable(priceRequest.getAvailable());
    price.setDiscountPercent(new Double(priceRequest.getDiscountPercent()));
    price.setDiscountPrice(new Double(priceRequest.getDiscountPrice()));
    price.setListInCents(new Double(priceRequest.getListInCents()));
    price.setSaleInCents(new Double(priceRequest.getSaleInCents()));
    price.setSeller(
        new Seller(priceRequest.getSeller().getId(), priceRequest.getSeller().getName()));
    return price;
  }
}
