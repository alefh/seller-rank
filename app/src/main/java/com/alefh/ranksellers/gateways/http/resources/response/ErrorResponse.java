package com.alefh.ranksellers.gateways.http.resources.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@JsonInclude(Include.NON_EMPTY)
public class ErrorResponse {

  private List<String> errors = new ArrayList<>();

  public ErrorResponse(final List<String> erros) {
    this.errors = erros;
  }

  public ErrorResponse addError(String error) {
    getErrors().add(error);
    return this;
  }
}
