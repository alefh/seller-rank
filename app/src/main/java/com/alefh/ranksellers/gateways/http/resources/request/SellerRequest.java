package com.alefh.ranksellers.gateways.http.resources.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class SellerRequest {

  @NotEmpty(message = "{validation.field.notnull}")
  private String id;

  @NotEmpty(message = "{validation.field.notnull}")
  private String name;
}
