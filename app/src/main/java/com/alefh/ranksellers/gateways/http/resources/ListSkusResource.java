package com.alefh.ranksellers.gateways.http.resources;

import static java.util.Objects.isNull;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ListSkusResource<T> {

  @Valid
  @NotEmpty(message = "{validation.field.notnull}")
  private List<T> skus;

  public List<T> getSkus() {
    if (isNull(skus)) {
      skus = new ArrayList<>();
    }
    return skus;
  }
}
