package com.alefh.ranksellers.usecases;

import com.alefh.ranksellers.domains.Sku;
import com.alefh.ranksellers.domains.RankedSellers;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class ProcessRankSellers {

  private SaveRankedSellers saveRankedSellers;

  public List<RankedSellers> execute(final String country, final String storeCode, final List<Sku> skus) {
    List<RankedSellers> listRankedSellers = new ArrayList<>();

    skus.forEach(
            sku -> {
              log.info("Processing rank of sellers for sku: {}", sku);

              RankedSellers rankedSellers = new RankedSellers();
              rankedSellers.setSku(sku.getSku());
              sku.getPrices().forEach(price -> rankedSellers.addSeller(price.getSeller()));
              saveRankedSellers.execute(rankedSellers);
              listRankedSellers.add(rankedSellers);
            });
    return listRankedSellers;
  }
}
