package com.alefh.ranksellers.usecases;

import com.alefh.ranksellers.domains.RankedSellers;
import com.alefh.ranksellers.gateways.RankedSellersGateway;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class SaveRankedSellers {

  private RankedSellersGateway rankedSellersGateway;

  @Async
  public void execute(final RankedSellers rankedSellers) {
    log.info("Saving ranked sellers: {}", rankedSellers);
    rankedSellersGateway.save(rankedSellers);
  }
}
