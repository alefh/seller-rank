package com.alefh.ranksellers;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
public class RankSellersApplication {
  public static void main(String[] args) {
    SpringApplication.run(RankSellersApplication.class, args);
  }
}
