package com.alefh.ranksellers.domains;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@ToString(exclude = {"lastModifiedDate"})
@Document(collection = "rankedSellers")
@TypeAlias("RankedSellers")
@CompoundIndexes({@CompoundIndex(name = "idx_sku_idSeller", def = "{'sku' : 1, 'sellers.id': 1}")})
public class RankedSellers implements Serializable {

  private static final long serialVersionUID = 520505866494234390L;

  @Getter @Setter @Id private String sku;

  @Getter private Set<Seller> sellers = new LinkedHashSet<>();

  @Getter @LastModifiedDate private LocalDateTime lastModifiedDate;

  public void addSeller(final Seller seller) {
    sellers.add(seller);
  }
}
