package com.alefh.ranksellers.domains;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Price {

  private Boolean available;
  private Double saleInCents;
  private Double listInCents;
  private Double discountPrice;
  private Double discountPercent;
  private Seller seller;
}
