package com.alefh.ranksellers.domains;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "sku")
public class Sku {

  private String sku;
  private Boolean available;
  private List<Price> prices;
}
