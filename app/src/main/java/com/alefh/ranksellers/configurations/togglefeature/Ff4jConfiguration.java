package com.alefh.ranksellers.configurations.togglefeature;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.ff4j.FF4j;
import org.ff4j.core.Feature;
import org.ff4j.core.FeatureStore;
import org.ff4j.mongo.store.FeatureStoreMongo;
import org.ff4j.mongo.store.PropertyStoreMongo;
import org.ff4j.property.store.PropertyStore;
import org.ff4j.web.ApiConfig;
import org.ff4j.web.FF4jDispatcherServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@ComponentScan(basePackages = "org.ff4j.aop")
public class Ff4jConfiguration {
  private static final String DEFAULT_CONSOLE = "/ff4j-console/*";

  @Bean
  public FF4j getFF4j(final FeatureStore featureStore, final PropertyStore propertyStore) {
    final FF4j ff4j = new FF4j();
    ff4j.setFeatureStore(featureStore);
    ff4j.setPropertiesStore(propertyStore);

    for (Features feature : Features.values()) {
      createIfNotExists(ff4j, feature);
    }

    return ff4j;
  }

  @Bean
  public FF4jDispatcherServlet getFF4JServlet(final FF4j ff4j) {
    final FF4jDispatcherServlet consoleServlet = new FF4jDispatcherServlet();
    consoleServlet.setFf4j(ff4j);
    return consoleServlet;
  }

  @Bean
  public ApiConfig getApiConfig(final FF4j ff4j) {
    final ApiConfig apiConfig = new ApiConfig();
    apiConfig.setAuthenticate(false);
    apiConfig.setAutorize(false);
    apiConfig.setFF4j(ff4j);
    return apiConfig;
  }

  @Bean
  public ServletRegistrationBean servletRegistrationBean(final FF4j ff4j) {
    return new ServletRegistrationBean(getFF4JServlet(ff4j), DEFAULT_CONSOLE);
  }

  @Bean
  public FeatureStore featureStore(MongoClient mongoClient, MongoTemplate mongoTemplate) {
    final MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoTemplate.getDb().getName());
    return new FeatureStoreMongo(mongoDatabase, "ff4j-features");
  }

  @Bean
  public PropertyStore propertyStore(MongoClient mongoClient, MongoTemplate mongoTemplate) {
    final MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoTemplate.getDb().getName());
    return new PropertyStoreMongo(mongoDatabase, "ff4j-properties");
  }

  private void createIfNotExists(final FF4j ff4j, final Features feature) {
    final String featureKey = feature.getKey();
    if (!ff4j.getFeatureStore().exist(featureKey)) {
      final Feature fp = new Feature(featureKey, feature.isDefaultValue());
      fp.setDescription(feature.getDescription());
      fp.setGroup(fp.getGroup());
      ff4j.createFeature(fp);
    }
  }
}
