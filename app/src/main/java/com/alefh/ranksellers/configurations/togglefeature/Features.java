package com.alefh.ranksellers.configurations.togglefeature;

import lombok.Getter;

@Getter
public enum Features {
  START_FEATURE("Name of feature", "Group", "Description", false);

  private final String key;
  private final String groupname;
  private final String description;
  private final boolean defaultValue;

  Features(
      final String key, final String group, final String description, final boolean defaultValue) {
    this.key = key;
    this.description = description;
    this.defaultValue = defaultValue;
    this.groupname = group;
  }
}
