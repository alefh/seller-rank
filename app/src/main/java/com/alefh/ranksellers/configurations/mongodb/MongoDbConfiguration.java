package com.alefh.ranksellers.configurations.mongodb;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(
  basePackages = {"com.alefh.ranksellers.gateways.mongodb.repositories"}
)
@EnableMongoAuditing
public class MongoDbConfiguration {}
