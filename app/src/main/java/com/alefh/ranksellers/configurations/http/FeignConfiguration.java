package com.alefh.ranksellers.configurations.http;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"com.alefh.ranksellers.gateways.http"})
public class FeignConfiguration {}
